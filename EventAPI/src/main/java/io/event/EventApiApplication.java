package io.event;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class EventApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EventApiApplication.class, args);
	}
	
	/* 공용으로 사용한 객체 이므로
	 * Bean으로 등록하여 사용 */
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

}
