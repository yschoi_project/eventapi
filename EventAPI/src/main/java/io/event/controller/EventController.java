package io.event.controller;

import java.net.URI;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import io.event.domain.Event;
import io.event.domain.EventDto;
import io.event.repository.EventRepository;

@Controller
@RequestMapping(value = "/api", produces = MediaTypes.HAL_JSON_VALUE)
public class EventController { 
	
	/* @Autowired를 이용한 EventRepositroy 주입 */
//	@Autowired
//	EventRepository eventRepository;
	
	/* 생성자를 이용한 bean 주입 */
	private final EventRepository eventRepository;
	private final ModelMapper modelMapper;

	/* 생성자를 이용한 주입 시 참고 사항 
	 * - 해당 Class의 생성자가 하나만 존재하고, 생성자로 받아오는 파라미터가 이미 Spring Container에 등록되어 있는 경우 @Autowired 생략 가능 
	 * - 단, Spring 4.3 부터 지원
	 * */
//	@Autowired (Spring 4.3 이상이므로 생략 해본다.)
	public EventController(EventRepository eventRepository, ModelMapper modelMapper) {
		 this.eventRepository = eventRepository;
		 this.modelMapper = modelMapper;
	}
	
	@PostMapping(value = "/events")
	public ResponseEntity createEvent() {
		URI createdUri = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(EventController.class).createEvent()).slash("{id}").toUri(); 
		return ResponseEntity.created(createdUri).build();
	}
	
	@PostMapping(value = "/events01")
	public ResponseEntity createEvent_01(@RequestBody Event event) {
		URI createdUri = WebMvcLinkBuilder.linkTo(EventController.class).slash("{id}").toUri();
		event.setId(10);
		return ResponseEntity.created(createdUri).body(event);
	}
	
	@PostMapping(value = "/events02")
	public ResponseEntity createEvent_Repositroy(@RequestBody Event event) {
		
		/* eventRepository.save를 호출하여 저장된 객체를 newEvent로 리턴 받는다. */
		Event newEvent = this.eventRepository.save(event);
		URI createdUri = WebMvcLinkBuilder.linkTo(EventController.class).slash(newEvent.getId()).toUri();
		return ResponseEntity.created(createdUri).body(event);
	}
	
	@PostMapping(value = "/events03")
	public ResponseEntity createEvent_Dto(@RequestBody EventDto eventDto) {
		
		/* 입력값 제한을 위한 Dto Class를 입력 파라미로 받은후, Event 객체로 값 설정 */
//		Event event = Event.builder()
//				.name(eventDto.getName())
//				.description(eventDto.getDescription())
//				.build();

		/* ModelMapper를 이용한 객체 매핑 
		 * 이때 객체 Mapping을 위해 java reflection이 발생하며, 
		 * 이는 java bean방식의 setter method를 이용하는 방법보다 비용이 조금 더 소모됨을 주의하자.
		 * 
		 * eventDto -> Event.class로 객체 mapping을 진행한다.
		 * */
		Event event = modelMapper.map(eventDto, Event.class);
		
		/* eventRepository.save를 호출하여 저장된 객체를 newEvent로 리턴 받는다. */
		Event newEvent = this.eventRepository.save(event);
		URI createdUri = WebMvcLinkBuilder.linkTo(EventController.class).slash(newEvent.getId()).toUri();
		return ResponseEntity.created(createdUri).body(event);
	}
	
}
