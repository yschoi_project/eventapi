package io.event.domain;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor /* 모든 argument를 가지는 생성자 추가 */
@NoArgsConstructor /* 기본 생성자 추가 */
@EqualsAndHashCode(of = {"id"})
@Entity
public class Event {
	
	@Id @GeneratedValue
    private Integer id;
    private String name;
    private String description;
    private LocalDateTime beginEnrollmentDateTime; /*Spring boot 2.1 -> JPA 3.2를 지원 LocalDateTime Mapping 지원*/
    private LocalDateTime closeEnrollmentDateTime;
    private LocalDateTime beginEventDateTime;
    private LocalDateTime endEventDateTime;
    private String location; // (optional) 온라인/오프라인 구분 필드
    private int basePrice; // (optional)
    private int maxPrice; // (optional)
    private int limitOfEnrollment;
    private boolean offline;
    private boolean free;
    
    /*
     * 기본값 : EnumType.ORDINAL
     * 변경값 : EnumType.STRING
     * 변경이유 : ORDINAL= Enum Class 내부의 값 순서에 따라서 0, 1, 2.. 숫자 값이 저장되는데,
     * 	Enum 내부 값 순서가 변경되어 매핑되는 숫자값이 꼬이는 것을 방지 하기 위해 STRING 형태로 저장
     * */
    @Enumerated(EnumType.STRING) /* Ordinal -> Enum Class 값의 순서에 따라서 0, 1, 2 .. */
    private EventStatus eventStatus = EventStatus.DRAFT;
    
}
