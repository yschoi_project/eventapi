package io.event.domain;

public enum EventStatus {
	DRAFT, PUBLISHED, BEGAN_ENROLLMEND, CLOSED_ENROLLMENT, STARTED,  ENDED;
}
