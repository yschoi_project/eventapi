package io.event.domain;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 * 2020=04-23
 * 입력값 제한을 위한 DTO class 생성
 * */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class EventDto {

    private String name;
    private String description;
    private LocalDateTime beginEnrollmentDateTime; /*Spring boot 2.1 -> JPA 3.2를 지원 LocalDateTime Mapping 지원*/
    private LocalDateTime closeEnrollmentDateTime;
    private LocalDateTime beginEventDateTime;
    private LocalDateTime endEventDateTime;
    private String location; // (optional) 온라인/오프라인 구분 필드
    private int basePrice; // (optional)
    private int maxPrice; // (optional)
    private int limitOfEnrollment;
    
}
