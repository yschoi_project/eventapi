package io.event.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.event.domain.Event;

public interface EventRepository extends JpaRepository<Event, Integer>{

}
