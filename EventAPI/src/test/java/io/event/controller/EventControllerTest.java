package io.event.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.event.domain.Event;
import io.event.domain.EventStatus;
import io.event.repository.EventRepository;

@RunWith(SpringRunner.class)
@WebMvcTest
public class EventControllerTest {

	/* MockMvc
	 *  - DispatcherServlet을 mocking하여 Request 생성 가능
	 *  Request 생성 하고 Response를 검증할 수 있는 Spring MVC Test의 핵심 Class
	 * */
	@Autowired
	MockMvc mockMvc;

	@Autowired
	ObjectMapper objectMapper;
	
	/*
	 * TC title : MockMVC를 통해 생성한 Request에 대한 Response의 status : 201 여부 Test
	 * TC content : Event객체 생성 및 MockMVC를 통해 Get 요청
	 * Request Content Type : 
	 * Expect response Content Type
	 * Http method : GET
	 * Http Request : 
	 * Expect result : Http Response status is equals to 201 
	 * Written by : choi_ys
	 * Reporting date : 2020-04-21 (화)
	 * */
//	@Test
	public void createEventByStatusResponseTest() throws Exception {
		/* Given */
		String name = "LunaSoft Dev-con";
		String description = "제 1 회 루나소프트 DevCon";
		LocalDateTime beginEnrollmentDateTime = LocalDateTime.of(2020, 4, 10, 00, 00);
		LocalDateTime closeEnrollmentDateTime = LocalDateTime.of(2020, 4, 15, 00, 00);
		LocalDateTime beginEventDateTime = LocalDateTime.of(2020, 4, 21, 16, 00);
		LocalDateTime endEventDateTime = LocalDateTime.of(2020, 4, 21, 17, 30);
		int basePrice = 0;
		int maxPrice = 0;
		int limitOfEnrollment = 0;
		String location = "서울특별시 강남구 테헤란로 325 어반벤치 빌딩 12F 루나소프트";
		
		Event event = Event.builder()
				.name(name)
				.description(description)
				.beginEnrollmentDateTime(beginEnrollmentDateTime)
				.closeEnrollmentDateTime(closeEnrollmentDateTime)
				.beginEventDateTime(beginEventDateTime)
				.endEventDateTime(endEventDateTime)
				.basePrice(basePrice)
				.maxPrice(maxPrice)
				.limitOfEnrollment(limitOfEnrollment)
				.location(location)
				.build();
		
		/* When */
		mockMvc.perform(post("/api/events/")
					.contentType(MediaType.APPLICATION_JSON_UTF8) /* Request contentType 명시 */
					.accept(MediaTypes.HAL_JSON) /* Response */
					.content(objectMapper.writeValueAsString(event))
				)
				/* Then */
				.andDo(print())	/* Response 출력 */
				.andExpect(status().isCreated()); /* Data 일치 확인 */
	}
	
	/*
	 * request : Request Body에 content 추가 및 201 리턴 여부 Test
	 * expect : MockMvc를 통해 생성한 Request에 대한 Response가 JSON/status:201 여부 Test
	 * result : 
	 * */
//	@Test
	public void createEventByIdResponseTest() throws Exception{
		/* Given */
		String name = "LunaSoft Dev-con";
		String description = "제 1 회 루나소프트 DevCon";
		LocalDateTime beginEnrollmentDateTime = LocalDateTime.of(2020, 4, 10, 00, 00);
		LocalDateTime closeEnrollmentDateTime = LocalDateTime.of(2020, 4, 15, 00, 00);
		LocalDateTime beginEventDateTime = LocalDateTime.of(2020, 4, 21, 16, 00);
		LocalDateTime endEventDateTime = LocalDateTime.of(2020, 4, 21, 17, 30);
		int basePrice = 0;
		int maxPrice = 0;
		int limitOfEnrollment = 0;
		String location = "서울특별시 강남구 테헤란로 325 어반벤치 빌딩 12F 루나소프트";
		
		Event event = Event.builder()
				.name(name)
				.description(description)
				.beginEnrollmentDateTime(beginEnrollmentDateTime)
				.closeEnrollmentDateTime(closeEnrollmentDateTime)
				.beginEventDateTime(beginEventDateTime)
				.endEventDateTime(endEventDateTime)
				.basePrice(basePrice)
				.maxPrice(maxPrice)
				.limitOfEnrollment(limitOfEnrollment)
				.location(location)
				.build();
		
		/* When */
		mockMvc.perform(post("/api/events01/")
					.contentType(MediaType.APPLICATION_JSON_VALUE) /* Request contentType 명시 */
					.accept(MediaTypes.HAL_JSON) /* Response */
					.content(objectMapper.writeValueAsString(event))
				)
				.andDo(print())
				
				/* Then */
				.andExpect(status().isCreated())
				.andExpect(jsonPath("id").exists());
	}
	
	
	@MockBean
	EventRepository eventRepository;
	
	@Test
	public void createEventByRepositroyResponseTest() throws Exception{
		/* Given */
		String name = "LunaSoft Dev-con";
		String description = "제 1 회 루나소프트 DevCon";
		LocalDateTime beginEnrollmentDateTime = LocalDateTime.of(2020, 4, 10, 00, 00);
		LocalDateTime closeEnrollmentDateTime = LocalDateTime.of(2020, 4, 15, 00, 00);
		LocalDateTime beginEventDateTime = LocalDateTime.of(2020, 4, 21, 16, 00);
		LocalDateTime endEventDateTime = LocalDateTime.of(2020, 4, 21, 17, 30);
		int basePrice = 0;
		int maxPrice = 0;
		int limitOfEnrollment = 0;
		String location = "서울특별시 강남구 테헤란로 325 어반벤치 빌딩 12F 루나소프트";
		
		Event event = Event.builder()
				.name(name)
				.description(description)
				.beginEnrollmentDateTime(beginEnrollmentDateTime)
				.closeEnrollmentDateTime(closeEnrollmentDateTime)
				.beginEventDateTime(beginEventDateTime)
				.endEventDateTime(endEventDateTime)
				.basePrice(basePrice)
				.maxPrice(maxPrice)
				.limitOfEnrollment(limitOfEnrollment)
				.location(location)
				.build();
		
		/* When */
		int id = 10;
		event.setId(id);
		Mockito.when(eventRepository.save(event)).thenReturn(event);
		
		mockMvc.perform(post("/api/events02/")
					.contentType(MediaType.APPLICATION_JSON_VALUE) /* Request contentType 명시 */
					.accept(MediaTypes.HAL_JSON) /* Response */
					.content(objectMapper.writeValueAsString(event))
				)
				/* Then */
				.andDo(print())
				.andExpect(status().isCreated())
				.andExpect(jsonPath("id").exists())
				.andExpect(header().exists(HttpHeaders.LOCATION))
				.andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaTypes.HAL_JSON_VALUE))
				;
	}
	
	/*
	 * Title : 입력값 제한하기 Controller Test
	 * */
//	@Test
	public void createEventByRepositroyResponseTest02() throws Exception{
		/* Given */
		int id = 100;
		String name = "LunaSoft Dev-con";
		String description = "제 1 회 루나소프트 DevCon";
		LocalDateTime beginEnrollmentDateTime = LocalDateTime.of(2020, 4, 10, 00, 00);
		LocalDateTime closeEnrollmentDateTime = LocalDateTime.of(2020, 4, 15, 00, 00);
		LocalDateTime beginEventDateTime = LocalDateTime.of(2020, 4, 21, 16, 00);
		LocalDateTime endEventDateTime = LocalDateTime.of(2020, 4, 21, 17, 30);
		int basePrice = 0;
		int maxPrice = 0;
		int limitOfEnrollment = 0;
		String location = "서울특별시 강남구 테헤란로 325 어반벤치 빌딩 12F 루나소프트";
		
		Event event = Event.builder()
				.id(id)
				.name(name)
				.description(description)
				.beginEnrollmentDateTime(beginEnrollmentDateTime)
				.closeEnrollmentDateTime(closeEnrollmentDateTime)
				.beginEventDateTime(beginEventDateTime)
				.endEventDateTime(endEventDateTime)
				.basePrice(basePrice)
				.maxPrice(maxPrice)
				.limitOfEnrollment(limitOfEnrollment)
				.location(location)
				.free(false)
				.offline(false)
				.build();

		/* When */
//		int id = 10;
//		event.setId(id);
		Mockito.when(eventRepository.save(event)).thenReturn(event);
		
		mockMvc.perform(post("/api/events02/")
					.contentType(MediaType.APPLICATION_JSON_VALUE) /* Request contentType 명시 */
					.accept(MediaTypes.HAL_JSON) /* Response */
					.content(objectMapper.writeValueAsString(event))
				)
				/* Then */
				.andDo(print())
				.andExpect(status().isCreated())
				.andExpect(jsonPath("id").exists())
				.andExpect(header().exists(HttpHeaders.LOCATION))
				.andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaTypes.HAL_JSON_VALUE))
				.andExpect(jsonPath("id").value(Matchers.not("100")))
				.andExpect(jsonPath("free").value(Matchers.not(true)))
				;
	}
	
	
	
	
}
