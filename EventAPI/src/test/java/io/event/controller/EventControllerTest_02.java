package io.event.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.event.domain.Event;
import io.event.domain.EventStatus;
import lombok.extern.slf4j.Slf4j;

@SpringBootTest(webEnvironment = WebEnvironment.MOCK)
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class EventControllerTest_02 {

	@Autowired
	MockMvc mockMvc;

	@Autowired
	ObjectMapper objectMapper;
	
	/*
	 * Title : 입력값 제한하기 TC
	 * */
//	@Test
	public void createEvent() throws Exception{
		/* Given */
		int id = 100;
		String name = "LunaSoft Dev-con";
		String description = "제 1 회 루나소프트 DevCon";
		LocalDateTime beginEnrollmentDateTime = LocalDateTime.of(2020, 4, 10, 00, 00);
		LocalDateTime closeEnrollmentDateTime = LocalDateTime.of(2020, 4, 15, 00, 00);
		LocalDateTime beginEventDateTime = LocalDateTime.of(2020, 4, 21, 16, 00);
		LocalDateTime endEventDateTime = LocalDateTime.of(2020, 4, 21, 17, 30);
		int basePrice = 0;
		int maxPrice = 0;
		int limitOfEnrollment = 0;
		String location = "서울특별시 강남구 테헤란로 325 어반벤치 빌딩 12F 루나소프트";
		
		Event event = Event.builder()
				.id(id)
				.name(name)
				.description(description)
				.beginEnrollmentDateTime(beginEnrollmentDateTime)
				.closeEnrollmentDateTime(closeEnrollmentDateTime)
				.beginEventDateTime(beginEventDateTime)
				.endEventDateTime(endEventDateTime)
				.basePrice(basePrice)
				.maxPrice(maxPrice)
				.limitOfEnrollment(limitOfEnrollment)
				.location(location)
				.free(true)
				.offline(false)
				.eventStatus(EventStatus.PUBLISHED)
				.build();
		
		
		mockMvc.perform(post("/api/events03/")
				.contentType(MediaType.APPLICATION_JSON_VALUE) /* Request contentType 명시 */
				.accept(MediaTypes.HAL_JSON) /* Response */
				.content(objectMapper.writeValueAsString(event))
				)
		/* Then */
		.andDo(print())
		.andExpect(status().isCreated())
		.andExpect(jsonPath("id").exists())
		.andExpect(header().exists(HttpHeaders.LOCATION))
		.andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaTypes.HAL_JSON_VALUE))
		.andExpect(jsonPath("id").value(Matchers.not("100")))
		.andExpect(jsonPath("free").value(Matchers.not(true)))
		.andExpect(jsonPath("eventStatus").value(EventStatus.DRAFT.name()))
		;
	}
	
	@Test
	public void createEvent_Bad_Request() throws Exception{
		/* Given */
		int id = 100;
		String name = "LunaSoft Dev-con";
		String description = "제 1 회 루나소프트 DevCon";
		LocalDateTime beginEnrollmentDateTime = LocalDateTime.of(2020, 4, 10, 00, 00);
		LocalDateTime closeEnrollmentDateTime = LocalDateTime.of(2020, 4, 15, 00, 00);
		LocalDateTime beginEventDateTime = LocalDateTime.of(2020, 4, 21, 16, 00);
		LocalDateTime endEventDateTime = LocalDateTime.of(2020, 4, 21, 17, 30);
		int basePrice = 0;
		int maxPrice = 0;
		int limitOfEnrollment = 0;
		String location = "서울특별시 강남구 테헤란로 325 어반벤치 빌딩 12F 루나소프트";
		
		/*SpringBoot에서 제공하는 JSON -> java Bean 객체 변환 시 unknown property를 수신할 경우
		 * application.properties ->  spring.jackson.deserialization.fail-on-unknown-properties=true
		 * 속성을 통해 unknown property에 대한 bad rqeust(400 error) 응답 처리가 가능하다.
		 * */
		Event event = Event.builder()
				.id(id)
				.name(name)
				.description(description)
				.beginEnrollmentDateTime(beginEnrollmentDateTime)
				.closeEnrollmentDateTime(closeEnrollmentDateTime)
				.beginEventDateTime(beginEventDateTime)
				.endEventDateTime(endEventDateTime)
				.basePrice(basePrice)
				.maxPrice(maxPrice)
				.limitOfEnrollment(limitOfEnrollment)
				.location(location)
				.free(true)
				.offline(false)
				.eventStatus(EventStatus.PUBLISHED)
				.build();
		
		
		mockMvc.perform(post("/api/events03/")
				.contentType(MediaType.APPLICATION_JSON_VALUE) /* Request contentType 명시 */
				.accept(MediaTypes.HAL_JSON) /* Response */
				.content(objectMapper.writeValueAsString(event))
				)
		/* Then */
		.andDo(print())
		.andExpect(status().isBadRequest())
		;
	}
}
