package io.event.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import lombok.extern.slf4j.Slf4j;

//@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
//@Slf4j
public class EventTest {
	
	@Test
	public void builderTest() {
		/* Given */
		String name = "Inflearn Spring REST API";
		String description = "REST API development wiht Spring";
		
		/* When */
		Event event = Event.builder()
				.name(name)
				.description(description)
				.build();
		
		/* Then */
		assertThat(event).isNotNull();
	}
	
	@Test
	public void javaBeanTest() {
		/* Given */
		String name = "Inflearn Spring REST API";
		String description = "REST API development wiht Spring";
		
		/* When */
		Event event = new Event();
		event.setName(name);
		event.setDescription(description);
		
		/* Then */
		assertThat(event.getName()).isEqualTo(name);
	}
	
}
